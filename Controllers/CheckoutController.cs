﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MusicStore.DatabaseContexts;
using MusicStore.Models;

namespace MusicStore.Controllers
{
    [Authorize(Roles = "Admin, User")]
    public class CheckoutController : Controller
    {
        private DatabaseContext _context;
        const string PromoCode = "FREE";

        public CheckoutController(DatabaseContext context)
        {
            _context = context;

        }


        public ActionResult AddressAndPayment()
        {
            return View();
        }

        [HttpPost]
        [ActionName("AddressAndPayment")]
        public async Task<ActionResult> AddressAndPaymentAsync(IFormCollection values)
        {
            var order = new Order();
            await TryUpdateModelAsync(order);

            try
            {
                if (string.Equals(values["PromoCode"], PromoCode,
                        StringComparison.OrdinalIgnoreCase) == false)
                {
                    return View(order);
                }
                else
                {
                    order.Username = User.Identity.Name;
                    order.OrderDate = DateTime.Now;

                    //Save Order
                    _context.Orders.Add(order);
                    _context.SaveChanges();

                    //Process the order
                    var cart = ShoppingCart.GetCart(HttpContext, _context);
                    cart.CreateOrder(order);

                    return RedirectToAction("Complete",
                        new { id = order.OrderId });
                }
            }
            catch
            {
                //Invalid - redisplay with errors
                return View(order);
            }
        }

        public ActionResult Complete(int id)
        {
            // Validate customer owns this order
            bool isValid = _context.Orders.Any(
                o => o.OrderId == id &&
                     o.Username == User.Identity.Name);

            if (isValid)
            {
                return View(id);
            }
            else
            {
                return View("Error");
            }
        }

    }
}