﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MusicStore.DatabaseContexts;
using MusicStore.Models;

namespace MusicStore.Controllers
{
    public class StoreController : Controller
    {
        private readonly DatabaseContext _context;

        public StoreController(DatabaseContext context)
        {
            _context = context;

        }

        public IActionResult Details(int id)
        {
            var album = _context.Albums.Include(z => z.Genre).Include(c => c.Artist).First(x => x.AlbumId == id);

            return View(album);
        }

        public IActionResult Index()
        {
            var genreModel = new List<Genre>
            {
                new Genre {Name = "Disco"},
                new Genre {Name = "Jazz"},
                new Genre {Name = "Rock"}
            };


            return View(genreModel);
        }

        public IActionResult Browse(string genre)
        {
            // Retrieve Genre and its Associated Albums from database
            var genreModel = _context.Genres.Include(x => x.Albums)
                .Single(g => g.Name == genre);

            return View(genreModel);
        }

 
    }

}
