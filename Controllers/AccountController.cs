﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MusicStore.DatabaseContexts;
using MusicStore.Models;

namespace MusicStore.Controllers
{
    public class AccountController : Controller
    {
        private DatabaseContext _context;

        public AccountController(DatabaseContext context)
        {
            _context = context;

        }

        private void MigrateShoppingCart(string userName)
        {
            // Associate shopping cart items with logged-in user
            var cart = ShoppingCart.GetCart(this.HttpContext, _context);

            cart.MigrateCart(userName);
            HttpContext.Session.SetString(ShoppingCart.CartSessionKey, userName);
        }

        //
        // GET: /Account/LogOn

        public ActionResult LogIn()
        {
            return View();
        }

        //
        // POST: /Account/LogOn

        [HttpPost]
        [ActionName("LogIn")]
        public async Task<ActionResult> LogOnAsync(AccountModel.LogOnModel model)
        {

            if (!string.IsNullOrEmpty(model.UserName) && string.IsNullOrEmpty(model.Password)) { 
                return RedirectToAction("LogIn");
            }

            if (ModelState.IsValid)
            {
                var userProfileDb = await _context.Accounts.
                    FirstOrDefaultAsync(a => a.UserName.Equals(model.UserName));

                if (userProfileDb == null)
                {
                    return RedirectToAction("LogIn");
                }

                ClaimsIdentity identity;


                if (userProfileDb.UserName == "gianni")
                {
                    //Create the identity for the user
                    identity = new ClaimsIdentity(new[] {
                        new Claim(ClaimTypes.Name, userProfileDb.UserName),
                        new Claim(ClaimTypes.Role, "Admin")
                    }, CookieAuthenticationDefaults.AuthenticationScheme);

                }
                else
                {
                    //Create the identity for the user
                    identity = new ClaimsIdentity(new[] {
                        new Claim(ClaimTypes.Name, userProfileDb.UserName),
                        new Claim(ClaimTypes.Role, "User")
                    }, CookieAuthenticationDefaults.AuthenticationScheme);
                }


                var principal = new ClaimsPrincipal(identity);

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                return RedirectToAction("Index", "Home");

            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            var login = HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            HttpContext.Response.Cookies.Delete("UserName");

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        [ActionName("Register")]
        public async Task<ActionResult> RegisterAsync(AccountModel.RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.ConfirmPassword == model.Password)
                {
                    await _context.Accounts.AddAsync(new Account
                    {
                        Password = model.Password,
                        UserName = model.UserName
                    });
                }

                await _context.SaveChangesAsync();

                return RedirectToAction("LogIn");


            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePassword

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Account/ChangePassword

        [Authorize]
        [HttpPost]
        [ActionName("ChangePassword")]
        public async Task<ActionResult> ChangePasswordAsync(AccountModel.ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                var account = await _context.Accounts.FirstOrDefaultAsync(x => x.UserName == HttpContext.User.Identity.Name);

                if (account != null && account.Password == model.OldPassword
                                    && model.NewPassword == model.ConfirmPassword)
                {
                    account.Password = model.NewPassword;

                    await _context.SaveChangesAsync();

                    return RedirectToAction("LogIn");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePasswordSuccess

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }
    }
}