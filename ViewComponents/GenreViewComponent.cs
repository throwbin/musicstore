﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MusicStore.DatabaseContexts;
using MusicStore.Models;

namespace MusicStore.ViewComponents
{
    public class GenreViewComponent : ViewComponent
    {
        private DatabaseContext _context;

        public GenreViewComponent(DatabaseContext context)
        {
            _context = context;

        }

        //
        // GET: /ShoppingCart/CartSummary
        public IViewComponentResult Invoke()
        {
            var genres = _context.Genres.ToList();

            return View(genres);
        }
    }
}
