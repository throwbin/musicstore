﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MusicStore.DatabaseContexts;
using MusicStore.Models;

namespace MusicStore.ViewComponents
{
    public class CartViewComponent : ViewComponent
    {
        private DatabaseContext _context;

        public CartViewComponent(DatabaseContext context)
        {
            _context = context;

        }

        //
        // GET: /ShoppingCart/CartSummary
        public IViewComponentResult Invoke()
        {
            var cart = ShoppingCart.GetCart(this.HttpContext, _context);

            var count = cart.GetCount();
            return View(count);
        }
    }
}
